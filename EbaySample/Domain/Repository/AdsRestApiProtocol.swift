//
//  AdsRestApiProtocol.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import UIKit

protocol AdsRestApiProtocol: class {

    //App Entities -> App Models
    
    func adDetails(id: Int,
                   completion: @escaping (Result<AdDetailsModel, Error>) -> Void)

    func adImage(url: String,
                 size: DomainEnums.ImageSize,
                 completion: @escaping (Result<UIImage, Error>) -> Void)
}
