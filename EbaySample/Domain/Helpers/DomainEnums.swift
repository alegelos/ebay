//
//  DomainEnums.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import Foundation

struct DomainEnums {
    
    enum ImageSize {
        case preview, full
    }
}
