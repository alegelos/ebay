//
//  AdDetailsModel.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import Foundation

struct AdDetailsModel {
    
    let id: Int
    let title: String
    let price: Price
    let visits: Int
    let address: Address
    let postedDateTime: Date
    let description: String
    let attributes: [Attributes]
    let features: [String]
    let pictures: [String]
    let documents: [Document]
    
}

// MARK: - Helping Structures

extension AdDetailsModel {
    
    struct Price {
        let currency: String
        let amount: Int
    }
    
    struct Address {
        let street: String
        let city: String
        let zipCode: String
        let longitude: String
        let latitude: String
    }
    
    struct Attributes {
        let label: String
        let value: String
        let unit: String
    }
    
    struct Document {
        let link: String
        let title: String
    }
    
}
