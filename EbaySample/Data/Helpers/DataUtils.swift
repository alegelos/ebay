//
//  DataUtils.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import Foundation

struct DataUtils {
    
    static func data(fromFile file: String, ext: String = "json") -> Data {
        guard let url = Bundle.main.url(forResource: file,
                                        withExtension: ext),
              let data = try? Data(contentsOf: url) else {
            //TODO: - log error
            return Data()
        }
        return data
    }
    
}
