//
//  EbayApiAdDetailsResponse.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import Foundation

struct EbayApiAdDetailsResponse: Decodable {
    
    let id: Int
    let title: String
    let price: Price
    let visits: Int
    let address: Address
    let postedDateTime: Date //TODO: decode date format
    let description: String
    let attributes: [Attributes]
    let features: [String]
    let pictures: [String]
    let documents: [Document]
    
}

// MARK: - Helping Structures

extension EbayApiAdDetailsResponse {
    
    struct Price: Decodable {
        let currency: String
        let amount: Int
    }
    
    struct Address: Decodable {
        let street: String
        let city: String
        let zipCode: String
        let longitude: String
        let latitude: String
        
        private enum CodingKeys : String, CodingKey {
            case street, city, longitude, latitude
            case zipCode = "zip_code"
        }
    }
    
    struct Attributes: Decodable {
        let label: String
        let value: String
        let unit: String
    }
    
    struct Document: Decodable {
        let link: String
        let title: String
    }
    
}

// MARK: - Private

extension EbayApiAdDetailsResponse {
    
    private enum CodingKeys: String, CodingKey {
        case id, title, price, visits, address, description, attributes, features, pictures, documents
        case postedDateTime = "posted-date-time"
    }
    
}
