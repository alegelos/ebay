//
//  EbayApi.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import Foundation
import Moya

enum EbayApi {
    case adDetails(id: Int)
    case adImage(url: String, size: ImageSize = .preview)
    
    enum ImageSize: String {
        case preview = "1"
        case full = "57"
    }
}

// MARK: - TargetType

extension EbayApi: TargetType {
    
    var baseURL: URL {
        let tempBaseURL: URL?
        switch self {
        case .adDetails:
            tempBaseURL = URL(string: "https://gateway.ebay-kleinanzeigen.de/mobile-api/")
        case .adImage(let url, let size):
            let sizedImageURL = url.replacingOccurrences(of: "{imageId}",
                                                         with: size.rawValue)
            tempBaseURL = URL(string: sizedImageURL)
        }
        
        guard let baseURL = tempBaseURL else {
            //TODO: log error, app should not crash.
            fatalError("Fail to get \(self) base URL")
        }
        return baseURL
    }
    
    var path: String {
        switch self {
        case .adDetails(let id):
            return "candidate/ads/\(id)"
        case .adImage:
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .adDetails, .adImage:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .adDetails(let id):
            return DataUtils.data(fromFile: "EbayApiAdSample\(id)", ext: "json")
        case .adImage:
            //TODO: add image sample data
            return Data()
        }
    }
    
    var task: Task {
        switch self {
        case .adDetails, .adImage:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .adDetails, .adImage:
            return ["Content-Type": "application/json"]
        }
    }
    
}
