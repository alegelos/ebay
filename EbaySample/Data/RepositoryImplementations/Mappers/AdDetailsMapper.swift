//
//  AdDetailsMapper.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import Foundation

struct AdDetailsMapper {
    
    static func adDetails(_ response: EbayApiAdDetailsResponse) -> AdDetailsModel {
        
        let price = AdDetailsModel.Price(currency: response.price.currency,
                                         amount: response.price.amount)
        
        let address = AdDetailsModel.Address(street: response.address.street,
                                             city: response.address.city,
                                             zipCode: response.address.zipCode,
                                             longitude: response.address.longitude,
                                             latitude: response.address.latitude)
        
        let attributes = response.attributes.map {
            AdDetailsModel.Attributes(label: $0.label,
                                      value: $0.value,
                                      unit: $0.unit)
        }
        
        let documents = response.documents.map {
            AdDetailsModel.Document(link: $0.link,
                                    title: $0.title)
        }
            
        
        return AdDetailsModel(id: response.id,
                       title: response.title,
                       price: price,
                       visits: response.visits,
                       address: address,
                       postedDateTime: response.postedDateTime,
                       description: response.description,
                       attributes: attributes,
                       features: response.features,
                       pictures: response.pictures,
                       documents: documents)
    }
    
}
