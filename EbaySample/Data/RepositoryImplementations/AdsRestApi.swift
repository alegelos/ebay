//
//  AdsRestApi.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import UIKit
import Moya

final class AdsRestApi: AdsRestApiProtocol {
    
    private let ebayProvider: MoyaProvider<EbayApi>
    
    init(_ provider: MoyaProvider<EbayApi>) {
        ebayProvider = provider
    }
    
    // App Entities -> DataSource API -> Response Models -> App Models
    
    func adDetails(id: Int, completion: @escaping (Result<AdDetailsModel, Error>) -> Void) {
        
        ebayProvider.request(.adDetails(id: id)) { result in
            switch result {
            case .success(let response):
                do {
                    let adDetailsResponse = try response.map(EbayApiAdDetailsResponse.self)
                    let adDetails = AdDetailsMapper.adDetails(adDetailsResponse)
                    completion(.success(adDetails))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
    }
    
    func adImage(url: String, size: DomainEnums.ImageSize, completion: @escaping (Result<UIImage, Error>) -> Void) {
        
        var apiImageSize: EbayApi.ImageSize {
            switch size {
            case .full:
                return EbayApi.ImageSize.full
            case .preview:
                return EbayApi.ImageSize.preview
            }
        }
        
        ebayProvider.request(.adImage(url: url, size: apiImageSize)) { result in
            
            //TODO :(
            
        }
    }
    
}
