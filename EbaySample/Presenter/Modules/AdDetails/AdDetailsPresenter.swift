//
//  AdDetailsPresenter.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import UIKit

protocol AdDetailsPresenterDelegate: class {
    func showAdSections(_ add: AdDetailsModel)
    func showAdImages(_ images: [UIImage])
}

protocol AdDetailsPresenterProtocol: class {
    func didTapImage(_ index: Int)
}

final class AdDetailsPresenter {
    
    weak var delegate: AdDetailsPresenterDelegate?
    
    private let adsRestApi: AdsRestApiProtocol
    
    init(adsRestApi: AdsRestApiProtocol) {
        self.adsRestApi = adsRestApi
    }
}

// MARK: - AdDetailsPresenter

extension AdDetailsPresenter: AdDetailsPresenterProtocol {
    
    func didTapImage(_ index: Int) {
        //TODO show image details
    }
    
}
