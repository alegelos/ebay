//
//  AdDetailsViewController.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import UIKit
import Moya

final class AdDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    //TODO: move to coordinator and get userName and pass from UserDataModel
    private var presenter: AdDetailsPresenterProtocol = {
        let provider = MoyaProvider<EbayApi>(plugins: [CredentialsPlugin { _ -> URLCredential? in
                return URLCredential(user: "candidate", password: "yx6Xz62y", persistence: .none)
            }
        ])
        let restApi = AdsRestApi(provider)
        
        return AdDetailsPresenter(adsRestApi: restApi)
    }()
    
}

// MARK: - AdDetailsPresenterDelegate

extension AdDetailsViewController: AdDetailsPresenterDelegate {
    
    func showAdSections(_ add: AdDetailsModel) {
        //TODO: update table data
    }
    
    func showAdImages(_ images: [UIImage]) {
        //TODO: show images
    }
    
}

// MARK: - UITableViewDataSource

extension AdDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //TODO
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //TODO
        return UITableViewCell()
    }
    
    //TODO: load table sections
}

// MARK: - UITableViewDelegate

extension AdDetailsViewController: UITableViewDelegate {
    //TODO: delegate touch events to presenter
}

// MARK: - Private

extension AdDetailsViewController {
    
    
}
