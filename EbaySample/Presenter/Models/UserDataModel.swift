//
//  UserDataModel.swift
//  EbaySample
//
//  Created by Alejandro Gelos on 4/27/21.
//

import Foundation

struct UserDataModel {
    let userName: String
    let password: String
}
